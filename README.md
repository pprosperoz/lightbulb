- Platform: Xcode
- Modify data.txt file with new values.
- Run the app with Xcode, notice that a grid is created using the values of txt file.
- Clic on "Light Up" to set the lightbulbs.
- Make sure all the rows have the same length.
- Example of an matrix with correct formart: 
[
[0,0,0,1,0,1,1,1],
[0,1,0,0,0,0,0,0],
[0,1,0,0,0,1,1,1],
[0,1,1,1,1,0,0,0],
[0,1,0,0,1,0,0,0],
[0,1,0,0,1,0,1,0],
[0,1,0,1,1,0,1,0],
[0,0,0,0,1,0,0,0],
[0,1,1,0,1,1,0,1],
[0,0,0,0,1,0,0,0],
[1,0,0,1,0,0,1,1],
[0,0,1,0,0,0,0,0]
]
