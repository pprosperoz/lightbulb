//
//  ViewController.swift
//  GCLightBulb
//
//  Created by Pilar Del Rosario Prospero Zeferino on 3/24/20.
//  Copyright © 2020 Pil. All rights reserved.
//

import UIKit

class CellModel {
    var value: Int // 0 = No wall, 1 = Wall
    var status: Int // 0 = Available, 1= Bulb, 2 = No Bulb

    init(value: Int) {
        self.value = value
        self.status = 0
    }

    func isAvailable() -> Bool {
        if status == 0 {
            return true
        }
        return false
    }

    func setBulb() {
        status = 1
    }

    func setNoBulb() {
        status = 2
    }
}

class ViewController: UIViewController {

    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var totalBulbsLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    private var matrix = Array<Array<CellModel>>()
    private var bulbsNeeded: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        self.collectionView?.collectionViewLayout = layout
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getData()
    }

    @IBAction func buttonWasClicked(_ sender: Any) {
        calculate()
        collectionView.reloadData()
        var sum = 0
        for row in matrix {
            let bulbsNeeded = row.filter { $0.status == 1 }
            sum += bulbsNeeded.count
        }
        totalBulbsLabel.text = "BULBS NEEDED: \(bulbsNeeded)"
    }

    func getData() {
        if let path = Bundle.main.path(forResource: "data", ofType: "txt") {
            do {
                  let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                  let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let data = jsonResult as? Array<Array<Int>> {
                    var correctFormat: Bool = true
                    for (index, row) in data.enumerated() {
                        let lenghtRow = row.count
                        guard let nextRow = data[safe:index+1] else {
                            break
                        }
                        if lenghtRow != nextRow.count {
                            correctFormat = false
                            break
                        }
                    }

                    if correctFormat {
                        for row in data {
                            var rowObjects = Array<CellModel>()
                            for cell in row {
                                rowObjects.append(CellModel(value:cell))
                            }
                            matrix.append(rowObjects)
                        }
                        collectionView.reloadData()
                    } else {
                        let alert = UIAlertController(title: "INVALID FORMAT", message: "Rows should have same lenght", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        present(alert, animated: true, completion: nil)
                    }

//                    matrix[0][0] = CellModel(value: 9,status: 11)
                  }
              } catch {
                let alert = UIAlertController(title: "INVALID FORMAT", message: "Please set up data.txt file with correct values", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                present(alert, animated: true, completion: nil)
                print(error.localizedDescription)
              }
        }
    }

    func calculate() {
        for (rowIndex, row) in matrix.enumerated() {
            for (colIndex, cell) in row.enumerated() {
                if cell.value == 1 || !cell.isAvailable() {
                    continue
                }
                let bulbPosition = getVerticalPosition(rowIndex: rowIndex, colIndex: colIndex)
                if bulbPosition.0 == rowIndex && bulbPosition.1 == colIndex {
                    if !isHorizontalyOptimal(rowIndex: rowIndex, colIndex: colIndex) {
                        continue
                    }
                }
                placeBulb(rowIndex: bulbPosition.0, colIndex: bulbPosition.1)
            }
        }
    }

    func isHorizontalyOptimal(rowIndex: Int, colIndex: Int) -> Bool {
        if let bottomCell = matrix[safe:rowIndex+1]?[colIndex], bottomCell.value == 0 {
            return true
        }
        let row = matrix[rowIndex]
        var available = 0
        for index in (colIndex..<row.count) {
            guard let nextCell = row[safe:index+1], nextCell.value == 0 else {
                break
            }
            if nextCell.isAvailable() {
                available = 1
            }
        }
        if available == 0 {
            return true
        }
        return false
    }

    func getVerticalPosition(rowIndex: Int, colIndex: Int) -> (Int, Int) {
        var position: (Int, Int) = (rowIndex, colIndex)
        for index in (rowIndex..<matrix.count) {
            guard let nextCell = matrix[safe:index+1]?[safe:colIndex], nextCell.value == 0 else {
                break
            }
            if !nextCell.isAvailable() {
                continue
            }
            var currentValue = getCellVerticalValue(rowIndex: position.0, colIndex: position.1)
            var nextValue = getCellVerticalValue(rowIndex: index+1, colIndex: colIndex)
            if nextValue > currentValue {
                position = (index+1, colIndex)
            } else if currentValue == 1 && nextValue == 1 {
                currentValue = getHorizontalIntersectionValue(rowIndex: position.0, colIndex: position.1)
                nextValue = getHorizontalIntersectionValue(rowIndex: index+1, colIndex: colIndex)
                if nextValue < currentValue {
                    position = (index+1, colIndex)
                }
            }
        }
        return position
    }

    func placeBulb(rowIndex: Int, colIndex: Int) {
        matrix[rowIndex][colIndex].setBulb()
        bulbsNeeded += 1
        let row = matrix[rowIndex]
        for index in (colIndex..<row.count) {
            guard let nextCell = row[safe:index+1], nextCell.value == 0 else {
                break
            }
            if !nextCell.isAvailable() {
                continue
            }
            nextCell.setNoBulb()
        }
        for index in (0..<colIndex).reversed() {
            guard let prevCell = row[safe:index], prevCell.value == 0 else {
                break
            }
            if !prevCell.isAvailable() {
                continue
            }
            prevCell.setNoBulb()
        }
        for index in (rowIndex..<matrix.count) {
            guard let bottomCell = matrix[safe:index+1]?[safe:colIndex], bottomCell.value == 0 else {
                break
            }
            if !bottomCell.isAvailable() {
                continue
            }
            bottomCell.setNoBulb()
        }
        for index in (0..<rowIndex).reversed() {
            guard let topCell = matrix[safe:index]?[safe:colIndex], topCell.value == 0 else {
                break
            }
            if !topCell.isAvailable() {
                continue
            }
            topCell.setNoBulb()
        }
    }

    func getHorizontalIntersectionValue(rowIndex: Int, colIndex: Int) -> Int {
        let row = matrix[rowIndex]
        var sum = 0
        for index in (colIndex..<row.count) {
            guard let nextCell = row[safe:index+1], nextCell.value == 0 else {
                break
            }
            sum += getCellHorizontalValue(rowIndex: rowIndex, colIndex: index+1)
        }
        for index in (0..<colIndex).reversed() {
            guard let prevCell = row[safe:index], prevCell.value == 0 else {
                break
            }
            sum += getCellHorizontalValue(rowIndex: rowIndex, colIndex: index)
        }
        print(sum)
        return sum
    }

    func getCellHorizontalValue(rowIndex: Int, colIndex: Int) -> Int {
        if let topCell = matrix[safe:rowIndex-1]?[colIndex], topCell.value == 0 {
            return 1
        }
        if let bottomCell = matrix[safe:rowIndex+1]?[colIndex], bottomCell.value == 0 {
            return 1
        }
        return 0
    }

    func getCellVerticalValue(rowIndex: Int, colIndex: Int) -> Int {
        if let leftCell = matrix[rowIndex][safe:colIndex-1], leftCell.value == 0 {
            return 1
        }
        if let rightCell = matrix[rowIndex][safe:colIndex+1], rightCell.value == 0 {
            return 1
        }
        return 0
    }
}

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        matrix.count
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let elements = matrix[safe: section] else {
            return 0
        }
        return elements.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "identifier", for: indexPath) as? RoomCollectionViewCell else {
            return UICollectionViewCell()
        }
        guard let elements = matrix[safe: indexPath.section], let status = elements[safe: indexPath.row] else {
            return cell
        }
        cell.setUpCell(status: status.value)
        if status.status == 1 {
            cell.valueLabel.backgroundColor = .yellow
        }
        if status.status == 2 {
            cell.valueLabel.backgroundColor = UIColor(red: 255.0/255.0, green: 250.0/255.0, blue: 205.0/255.0, alpha: 1.0)
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        guard let elements = matrix[safe: indexPath.section] else {
            return CGSize(width: 0, height: 0)
        }
        let numberOfItemsPerRow:CGFloat = CGFloat(elements.count)

        if let collection = self.collectionView{
            let width = collection.bounds.width/numberOfItemsPerRow
            return CGSize(width: width, height: width)
        }else{
            return CGSize(width: 0, height: 0)
        }
    }
}

extension Array {
    subscript (safe index: Index) -> Element? {
        if self.count > index && index >= 0 {
            return self[index]
        } else {
            return nil
        }
    }
}

