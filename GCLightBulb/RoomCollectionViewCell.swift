//
//  RoomCollectionViewCell.swift
//  GCLightBulb
//
//  Created by Pilar Del Rosario Prospero Zeferino on 3/25/20.
//  Copyright © 2020 Pil. All rights reserved.
//

import Foundation
import UIKit

class RoomCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var valueLabel: UILabel!

    override public func awakeFromNib() {
        super.awakeFromNib()
        contentView.layer.borderWidth = 1
        contentView.layer.borderColor = UIColor.black.cgColor
    }

    override public func prepareForReuse() {
        super.prepareForReuse()
        valueLabel.text = String(format: "%d", 1)
        valueLabel.backgroundColor = .lightGray
    }

    func setUpCell(status: Int) {
        valueLabel.text = String(format: "%d", status)
        if status == 0 {
            valueLabel.backgroundColor = .white
        }
    }
}
